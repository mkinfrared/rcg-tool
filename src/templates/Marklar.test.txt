import { render } from "@testing-library/react";

import { Marklar } from "./Foobar";

describe("<Marklar />", () => {
  const Component = <Marklar />;

  it("should be defined", () => {
    expect(Marklar).toBeDefined();
  });

  it("should match the snapshot", () => {
    const { container } = render(Component);

    expect(container).toMatchSnapshot();
  });

  it("should contain a data test id", () => {
    const { getByTestId } = render(Component);
    const element = getByTestId("Marklar");

    expect(element).toBeDefined();
  });
});
