const getCamelCase = (value: string) => {
  const result = value
    .split("-")
    .map((chunk) => chunk.charAt(0).toUpperCase() + chunk.slice(1))
    .join("");

  return result;
};

export default getCamelCase;
