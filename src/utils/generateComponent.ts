import type { Options } from "../index";

import createComponentFile from "./createComponentFile";
import createIndexFile from "./createIndexFile";
import createIndexMockFile from "./createIndexMockFile";
import createMockFile from "./createMockFile";
import createStoriesFile from "./createStoriesFile";
import createStoryStyle from "./createStoryStyle";
import createStylesFile from "./createStylesFile";
import createTestFile from "./createTestFile";
import createTypeFile from "./createTypeFile";
import logger from "./logger";
import writeFile from "./writeFile";

const generateComponent = async ({ component, storybook = 7 }: Options) => {
  logger.info("Generating component...");

  console.log("STORYBOOK", storybook);

  const dirNames = component.split("/");
  const inputName = dirNames.pop() ?? "component";

  if (!dirNames.length) {
    dirNames.push("components");
  }

  const fileName = inputName;
  const compDir = fileName;
  const mockDir = `${compDir}/__mocks__`;

  try {
    logger.info("Creating type file...");

    let data = await createTypeFile(fileName);

    writeFile({ ...data, dirNames, compDir });

    logger.success("Created type file");

    logger.info("Creating styles file...");

    data = await createStylesFile(fileName);

    writeFile({ ...data, dirNames, compDir });

    logger.success("Created styles file");

    logger.info("Creating component file...");

    data = await createComponentFile(fileName);

    writeFile({ ...data, dirNames, compDir });

    logger.success("Created component file");

    logger.info("Creating stories file...");

    data = await createStoriesFile(dirNames, fileName, storybook);

    writeFile({ ...data, dirNames, compDir });

    logger.success("Created stories file");

    logger.info("Creating stories style...");

    data = await createStoryStyle();

    writeFile({ ...data, dirNames, compDir });

    logger.success("Created stories style");

    logger.info("Creating test file...");

    data = await createTestFile(fileName);

    writeFile({ ...data, dirNames, compDir });

    logger.success("Created test file");

    logger.info("Creating index file...");

    data = await createIndexFile(fileName);

    logger.success("Created index file");

    writeFile({ ...data, dirNames, compDir });

    logger.info("Creating mock file...");

    data = await createMockFile(fileName);

    logger.success("Created mock file");

    writeFile({ ...data, dirNames, compDir: mockDir });

    logger.info("Creating index mock file...");

    data = await createIndexMockFile(fileName);

    logger.success("Created index mock file");

    writeFile({ ...data, dirNames, compDir: mockDir });

    logger.success("DONE");
  } catch (e) {
    if (e instanceof Error) {
      logger.error(e.message);
    }
  }
};

export default generateComponent;
