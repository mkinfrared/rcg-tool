import { COMPONENT_NAME, FILE_NAME } from "./constants";
import getCamelCase from "./getCamelCase";
import readFile from "./readFile";

const createComponentFile = async (input: string) => {
  const componentName = getCamelCase(input);
  const file = await readFile("Marklar.txt");
  const componentRegex = new RegExp(COMPONENT_NAME, "g");
  const fileRegex = new RegExp(FILE_NAME, "g");
  const tempContent = file.replace(componentRegex, componentName);
  const content = tempContent.replace(fileRegex, input);
  const fileName = `${input}`;
  const ext = "tsx";

  return { fileName, content, ext };
};

export default createComponentFile;
