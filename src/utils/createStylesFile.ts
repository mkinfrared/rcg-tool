import { COMPONENT_NAME } from "./constants";
import getCamelCase from "./getCamelCase";
import readFile from "./readFile";

const createStylesFile = async (input: string) => {
  const componentName = getCamelCase(input);
  const file = await readFile("Marklar.module.txt");
  const regex = new RegExp(COMPONENT_NAME, "g");
  const content = file.replace(regex, componentName);
  const fileName = `${input}.module`;
  const ext = "scss";

  return { content, ext, fileName };
};

export default createStylesFile;
