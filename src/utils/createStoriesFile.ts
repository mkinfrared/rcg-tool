import { COMPONENT_NAME, FILE_NAME } from "./constants";
import getCamelCase from "./getCamelCase";
import readFile from "./readFile";

const createOldStorybookTemplate = async (
  dirNames: string[],
  input: string,
) => {
  const file = await readFile("Marklar.old.stories.txt");
  const fixedFile = file.replace(/component:/i, "component:");
  const dirName = dirNames[dirNames.length - 1];

  const value = fixedFile.replace(
    'title: "UI/Marklar"',
    `title: "${dirName.toUpperCase()}/${input}"`,
  );

  const componentName = getCamelCase(input);
  const componentRegex = new RegExp(COMPONENT_NAME, "g");
  const fileRegex = new RegExp(FILE_NAME, "g");
  const tempContent = value.replace(componentRegex, componentName);
  const content = tempContent.replace(fileRegex, input);
  const fileName = `${input}.stories`;
  const ext = "tsx";

  return { fileName, content, ext };
};

const createStorybookTemplate = async (dirNames: string[], input: string) => {
  const file = await readFile("Marklar.stories.txt");
  const fixedFile = file.replace(/component:/i, "component:");
  const dirName = dirNames[dirNames.length - 1];

  const value = fixedFile.replace(
    'title: "UI/Marklar"',
    `title: "${dirName.toUpperCase()}/${input}"`,
  );

  const componentName = getCamelCase(input);
  const componentRegex = new RegExp(COMPONENT_NAME, "g");
  const fileRegex = new RegExp(FILE_NAME, "g");
  const tempContent = value.replace(componentRegex, componentName);
  const content = tempContent.replace(fileRegex, input);
  const fileName = `${input}.stories`;
  const ext = "tsx";

  return { fileName, content, ext };
};

const createStoriesFile = (
  dirNames: string[],
  input: string,
  storybookVersion: number,
) => {
  if (storybookVersion < 7) {
    return createOldStorybookTemplate(dirNames, input);
  }

  return createStorybookTemplate(dirNames, input);
};

export default createStoriesFile;
