import readFile from "./readFile";

const createStoryStile = async () => {
  const content = await readFile("Story.module.txt");
  const fileName = "Story.module";
  const ext = "scss";

  return { fileName, content, ext };
};

export default createStoryStile;
