#!/usr/bin/env node

import yargs from "yargs";

import generateComponent from "./utils/generateComponent";

export type Options = {
  component: string;
  storybook?: number;
};

const start = () => {
  const options = yargs
    .usage("Usage: -c <component>")
    .option("component", {
      alias: "c",
      describe: "Component name",
    })
    .option("storybook", {
      alias: "s",
      describe: "Storybook version",
    })
    .demandOption(["component"], "Please provide a component name")
    .help().argv as Options;

  generateComponent(options);
};

start();
