# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 0.5.0 (2023-11-07)


### Features

* replace default export with named export ([e3edc09](https://github.com/mkinfrared/rcg-tool/commit/e3edc09735c062d4e125333806da786d1f25f4a7))

## 0.4.0 (2023-07-13)


### Features

* support storybook 7 templates ([bb93e22](https://github.com/mkinfrared/rcg-tool/commit/bb93e22aa534d483966c4170d52f708241c73d1c))


### Bug Fixes

* incorrect class name in tsx template ([5cd63cf](https://github.com/mkinfrared/rcg-tool/commit/5cd63cf96891f57d7f267de78119fe2499ed0491))

### 0.3.1 (2023-06-20)


### Features

* init ([7f81f1b](https://github.com/mkinfrared/rcg-tool/commit/7f81f1bfeba52718ed53f5b2b4aad7d66267cb9e))


### Bug Fixes

* incorrect class name in tsx template ([ded84f8](https://github.com/mkinfrared/rcg-tool/commit/ded84f8b53dd2e7d51e07515bdd6a16681242925))

### 0.2.3 (2022-10-11)


### Features

* add story styles ([9d33dfd](https://github.com/mkinfrared/rcg-tool/commit/9d33dfdb56ce86568e23c7c69d79b7b91f0f0b81)), closes [#10](https://github.com/mkinfrared/rcg-tool/issues/10)
* init ([7f81f1b](https://github.com/mkinfrared/rcg-tool/commit/7f81f1bfeba52718ed53f5b2b4aad7d66267cb9e))
* remove react memo ([7a01fde](https://github.com/mkinfrared/rcg-tool/commit/7a01fde0f084fa0feb473061618e773a67568b47))

## 0.2.2 (2022-05-14)


### Features

* add story styles ([9d33dfd](https://github.com/mkinfrared/rcg-tool/commit/9d33dfdb56ce86568e23c7c69d79b7b91f0f0b81)), closes [#10](https://github.com/mkinfrared/rcg-tool/issues/10)
* init ([7f81f1b](https://github.com/mkinfrared/rcg-tool/commit/7f81f1bfeba52718ed53f5b2b4aad7d66267cb9e))

## 0.2.1 (2022-02-19)


### Features

* add data-testid to Component ([ca1fbc9](https://github.com/mkinfrared/rcg-tool/commit/ca1fbc94007dc169745e5d6d471d037aaf859a4e))
* adds mock files ([74ac9a5](https://github.com/mkinfrared/rcg-tool/commit/74ac9a50d9e8840a1f3ea2c650b9fa5dff1065ea)), closes [#8](https://github.com/mkinfrared/rcg-tool/issues/8)

## 0.2.0 (2021-11-22)


### Features

* adds mock files ([abbf345](https://github.com/mkinfrared/rcg-tool/commit/abbf3457ad1671be70a94055e13c5b84a6f852d7)), closes [#8](https://github.com/mkinfrared/rcg-tool/issues/8)
* init ([7f81f1b](https://github.com/mkinfrared/rcg-tool/commit/7f81f1bfeba52718ed53f5b2b4aad7d66267cb9e))


### Bug Fixes

* strings to use double quotes ([c55685b](https://github.com/mkinfrared/rcg-tool/commit/c55685ba8994a31be625031c3902f01e825bcc4f)), closes [#7](https://github.com/mkinfrared/rcg-tool/issues/7)

### [0.1.9](https://github.com/mkinfrared/rcg-tool/compare/v0.1.8...v0.1.9) (2021-07-25)


### Bug Fixes

* strings to use double quotes ([5e39303](https://github.com/mkinfrared/rcg-tool/commit/5e3930383aaa4b52d9a617da036a4676f9b26d69)), closes [#7](https://github.com/mkinfrared/rcg-tool/issues/7)

### 0.1.8 (2021-07-04)


### Features

* init ([7f81f1b](https://github.com/mkinfrared/rcg-tool/commit/7f81f1bfeba52718ed53f5b2b4aad7d66267cb9e))


### Bug Fixes

* fix test description ([393b234](https://github.com/mkinfrared/rcg-tool/commit/393b234b10b67673f3c55b87f24a4b121fbb9dcf)), closes [#6](https://github.com/mkinfrared/rcg-tool/issues/6)

### 0.1.7 (2021-04-07)


### Features

* add subtitle for story files ([510ba54](https://github.com/mkinfrared/rcg-tool/commit/510ba5477f3bdac13a83808608d8f7899dd30101)), closes [#3](https://github.com/mkinfrared/rcg-tool/issues/3)
* create a component with any directory depth ([21b3b83](https://github.com/mkinfrared/rcg-tool/commit/21b3b83d3ac9f0d232af8cf3c9403493d478d033))
* init ([7f81f1b](https://github.com/mkinfrared/rcg-tool/commit/7f81f1bfeba52718ed53f5b2b4aad7d66267cb9e))

### [0.1.6](https://github.com/mkinfrared/react-component-generator/compare/v0.1.1...v0.1.6) (2021-04-04)


### Features

* change default prop value ([#14](https://github.com/mkinfrared/react-component-generator/issues/14)) ([5e4eebb](https://github.com/mkinfrared/react-component-generator/commit/5e4eebb3d2efb5fdb64f20746ca6bc50a28c49c3))

### [0.1.5](https://github.com/mkinfrared/react-component-generator/compare/v0.1.1...v0.1.5) (2021-04-04)


### Bug Fixes

* fix story meta ([#10](https://github.com/mkinfrared/react-component-generator/issues/10)) ([b12ab94](https://github.com/mkinfrared/react-component-generator/commit/b12ab94c5b241a814c2f3ce9000bb65f2a9a9911))

### [0.1.4](https://github.com/mkinfrared/react-component-generator/compare/v0.1.1...v0.1.4) (2021-04-04)


### Bug Fixes

* fix component prop name ([#7](https://github.com/mkinfrared/react-component-generator/issues/7)) ([27b6b92](https://github.com/mkinfrared/react-component-generator/commit/27b6b923cae9339aadd9b316f57c28d5e4a863f7))

### [0.1.3](https://github.com/mkinfrared/react-component-generator/compare/v0.1.2...v0.1.3) (2021-04-04)


### Bug Fixes

* fix component prop name ([#7](https://github.com/mkinfrared/react-component-generator/issues/7)) ([3e21eba](https://github.com/mkinfrared/react-component-generator/commit/3e21eba9dcc8de2e0d38e94756d76c3a96cd58ee))

### [0.1.2](https://github.com/mkinfrared/react-component-generator/compare/v0.1.1...v0.1.2) (2021-04-04)


### Bug Fixes

* fix package name ([a6d4704](https://github.com/mkinfrared/react-component-generator/commit/a6d4704259944b8d2476d1d90893a0bcbe43b6d5))

### 0.1.1 (2021-04-04)


### Features

* add styles to stories ([38b8e3f](https://github.com/mkinfrared/react-component-generator/commit/38b8e3f5a8d92fc6338fe6ecc26246103da65f94))
* init ([7f81f1b](https://github.com/mkinfrared/react-component-generator/commit/7f81f1bfeba52718ed53f5b2b4aad7d66267cb9e))
